# Coding Task
## Gitlab
```bash
git clone git@gitlab.com:Seb-dev/codingtask.git
```

## Docker
[Docker main site](https://www.docker.com/)

Build docker image

```bash
cd codingtask
```

We will call the container "codingtest"

```bash
docker build -t codingtest .
```
Once the image is built, it's time to launch the container. (we will map the port from the local to the distant server 4000 => 4000)
```bash
docker run -p 4000:4000 codingtest
```
Get the container infos
```bash
docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                              NAMES
64ac52e6d92a        codingtest          "npm start"         3 minutes ago       Up 3 minutes        0.0.0.0:4000->4000/tcp, 8080/tcp   pedantic_bartik
```
Access the container
```bash
docker exec -it pedantic_bartik sh
```
launch the tests
```bash
npm test
```

Check the generated files
```bash
ls -la upload/
```

The container should be up and running and the program started (and hopefully, the tests passed successfully).

-------
Stop the container
```bash
docker stop pedantic_bartik
```

-------

I'm using:
* npm 5.4.2
* node v8.7.0
* the operating system [Alpine](https://www.docker.com/) which is light, modern and secure
* [Express Js](https://expressjs.com/) which is a stable and robust Js framework
* [CORS Express middleware](https://github.com/expressjs/cors)
* [Moment Js](https://momentjs.com/) Which is an helpful tool for date manipulation
* [Moment Business Days Js](https://github.com/kalmecak/moment-business-days) another helpful tool for date manipulation, specializing in business date manipulation

And for the Dev dependencies
* [chai](http://chaijs.com/) a BDD / TDD assertion library
* [mocha](https://mochajs.org/) test framework
* [morgan](https://github.com/expressjs/morgan) a nice log and monitoring middleware
* [standard](https://standardjs.com/) To stay up to date with the coding standard
* [supertest](https://github.com/visionmedia/supertest) Super-agent driven library

## Limitation
Due to the time factor of the coding task, the following limitations have been identified and could be addressed (added in the backlog) in a future sprint

* The software will work on a period of 1 month. Adding a range to be abble to request different period (weeks, months, year) shouldn't be to difficult.

* The date format (default 'DD-MM-YYYY') can't be changed by the operator or the config file.

* The employees days off/holidays are single dates, we might want to add the possibility to accept date range

* Add a token for api authentification, ex JWT

* Check if the days Off are in the contractual limit

* Add multi-company management for subsidiaries


# Optional
For the design of a push service, regardless of the interface, I would go with [Apache artemis](https://activemq.apache.org/artemis/) which is a great broadcast manager (replacing apache ActiveMq/Apollo) and a [Stomp 1.2](https://stomp.github.io/index.html) client which can be implemented on any platforms / languages.
Apache Artemis has multiple security schemes to choose from (LDAP, role base, user credentials based, JAAS Security Manager, Kerberos, ssl, ..). Furthermore the access to queues and topics can be restricted allowing a very modular security policy.

# Timing for the Coding Task
* 1 hour : Install and config docker
* 3 hours : Design and architecture
* 8 hours : Coding and refactoring
* 2 hours : Testing
* 3 hours : Coding the tests
* 2 hours : Doc & git
* 1 hour : Reviews

Total : 20 hours and a lot of coffee
