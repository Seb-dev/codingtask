'use strict'

const request = require('supertest')
const app = require('../api')
const chai = require('chai')
const fs = require('fs')

var expect = chai.expect

describe('Check if API Available', function () {
  it('should return version number', function (done) {
    request(app)
      .get('/')
      .end(function (err, res) {
        expect(res.body.version).to.be.ok
        expect(res.statusCode).to.be.equal(200)
        done()
      })
  })
})

describe('Check the api for a code 200, version and that the generated file exists', function () {
  it('API generate Work Events from config - Estonia, C001', function (done) {
    request(app)
        .get('/config/C001') // Test period of december to visualy check holidays
        .end(function (err, res) {
          expect(res.body.version).to.be.ok
          expect(res.body.file).to.be.ok
          expect(res.statusCode).to.be.equal(200)
          expect(fs.existsSync('upload/ee_C001.json')).to.be.true
          done()
        })
  }),
  it('API generate Work Events from config - Finland, C002', function (done) {
    request(app)
      .get('/config/C002') // Test period of december to visualy check holidays
      .end(function (err, res) {
        expect(res.body.version).to.be.ok
        expect(res.body.file).to.be.ok
        expect(res.statusCode).to.be.equal(200)
        expect(fs.existsSync('upload/fi_C002.json')).to.be.true
        done()
      })
  }),
  it('API generate Work Events from URL - Estonia, C001', function (done) {
    request(app)
      .get('/ee/C001/01-12-2017') // Test period of december to visualy check holidays
      .end(function (err, res) {
        expect(res.body.version).to.be.ok
        expect(res.body.file).to.be.ok
        expect(res.statusCode).to.be.equal(200)
        expect(fs.existsSync('upload/ee_C001.json')).to.be.true
        done()
      })
  }),
  it('API generate Work Events from URL - Finland, C002', function (done) {
    request(app)
      .get('/fi/C002/01-12-2017') // Test period of december to visualy check holidays
      .end(function (err, res) {
        expect(res.body.version).to.be.ok
        expect(res.body.file).to.be.ok
        expect(res.statusCode).to.be.equal(200)
        expect(fs.existsSync('upload/fi_C002.json')).to.be.true
        done()
      })
  })
})

describe('Check the api for a code 200 and that the generated file does not comply (country file missing)', function () {
  it('API generate Work Events from config - France, T000', function (done) {
    request(app)
        .get('/config/T000') // Test period of december to visualy check holidays
        .end(function (err, res) {
          expect(res.statusCode).to.be.equal(500)
          expect(fs.existsSync('upload/fr_C001.json')).to.not.be.true
          done()
        })
  })
})
