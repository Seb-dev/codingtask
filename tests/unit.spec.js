'use strict'

// const sinon = require('sinon')
const assert = require('chai').assert
const moment = require('moment-business-days')
const workEventsUtils = require('../api/controllers/workEventsController.js')

describe('Check workingDays function', function () {
  it('workingDays function should filter the working days and remove the Estonian bank holidays - Total 19', function (done) {
    var getWorkingDays = workEventsUtils.workingDays('ee', '01-12-2017', '12')
    assert.equal(19, getWorkingDays.length)
    done()
  }),
  it('workingDays function should filter the working days and remove the Estonian bank holidays - total 19 not 21', function (done) {
    var getWorkingDays = workEventsUtils.workingDays('ee', '01-12-2017', '12')
    assert.notEqual(21, getWorkingDays.length)
    done()
  }),
    it('workingDays function should filter the working days and remove the Finnish bank holidays - total 18', function (done) {
      var getWorkingDays = workEventsUtils.workingDays('fi', '01-12-2017', '12')
      assert.equal(18, getWorkingDays.length)
      done()
    })
})

describe('Check formatEmployees function', function () {
  it('formatEmployees function should filter the employee list and have the same companyID (T000) as the one provided (T000)', function (done) {
    var employeesFormating = workEventsUtils.formatEmployees({country: 'ee', company: 'T000', from: '01-12-2017', period: '12'})
    assert.equal('T000', employeesFormating.companyID)
    done()
  }),
  it('formatEmployees function should filter the employee list and does not have the same companyID (T000) as the one provided (T001)', function (done) {
    var employeesFormating = workEventsUtils.formatEmployees({country: 'ee', company: 'T001', from: '01-12-2017', period: '12'})
    assert.notEqual('T000', employeesFormating.companyID)
    done()
  })
})

describe('Check workingDaysPerEmployees function', function () {
  it('workingDaysPerEmployees function should remove the employee Days Off from the working days list - total 17 not 19', function (done) {
    var employee = {
      'id': 'E001',
      'name': {
        'firstname': 'Sebastien',
        'lastname': 'Deschamps'
      },
      'country': 'ee',
      'company': {
        'name': 'Company One',
        'id': 'C001'
      },
      'contract': {
        'period': {
          'count': 1,
          'time': 'month'
        },
        'currency': 'EUR',
        'hourlyRate': 15,
        'hours': 8,
        'paymentDate': 27,
        'joined': '2017-09-10'
      },
      'leave': {
        'total': 20,
        'special': 0,
        'daysOff': [
          '10-10-2017',
          '04-12-2017',
          '11-12-2017'
        ]
      }
    }
    var employeeWorkingDay = workEventsUtils.workingDaysPerEmployees(employee, '01-12-2017', '12')
    assert.equal(2, employeeWorkingDay.leave)
    assert.equal(17, employeeWorkingDay.total)
    assert.notEqual(19, employeeWorkingDay.total)
    done()
  })
})
