'use strict'

module.exports = function (app) {
  var workEvents = require('./controllers/workEventsController')

    // todoList Routes
  app.route('/')
        .get(workEvents.getHome)

    // app.route('/employees/create') //Out of scope
    // .put(workEvents.updateEmployee); //Out of scope
    // app.route('/employees/remove') //Out of scope
    // .delete(workEvents.deleteEmployee); //Out of scope

  // Generate the work events from the URL
  app.route('/:country/:company/:from')
        .get(workEvents.generationFromUrl)

  // Generate the work events from the config file
  app.route('/config/:configName')
        .get(workEvents.generationFromConfig)
}
