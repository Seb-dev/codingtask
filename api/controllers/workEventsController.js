'use strict'
// Constants
const moment = require('moment-business-days')
const fs = require('fs')

// API home
exports.getHome = function (req, res, next) {
  res.json({version: '1.0', msg: 'cloudator coding task'})
}

// get Working events in the choosen period
exports.workingDays = function (country, from, period) {
  try {
    var getHolidays = require('../../data/holidays/' + country + '.json')
    // define the formated business days reference
    var formatedBD = []
    // get the business days for the coosen period
    var businessDays = moment(from, 'DD-MM-YYYY').monthBusinessDays()
    businessDays.forEach(function (element) {
      // create the formatted business day reference
      formatedBD.push(moment(element, 'DD-MM-YYYY').local(country).format('DD-MM-YYYY'))
    })
    // get holidays from the choosen period and remove them from the business day reference
    getHolidays.holidays.forEach(function (element) {
      if (element.month === period) {
        var check = moment(from, 'DD-MM-YYYY')
        var periodYear = check.format('Y')
        var removeDay = moment(element.day + '-' + element.month + '-' + periodYear, 'DD-MM-YYYY').format('DD-MM-YYYY')
        var i = formatedBD.indexOf(removeDay)
        if (i !== -1) {
          // Remove the holidays from the Business Days
          formatedBD.splice(i, 1)
        }
      }
    })
    return formatedBD
  } catch (e) {
    throw e
  }
}

exports.workingDaysPerEmployees = function (element, from, period) {
  try {
    // Create the work events list for the choosen period
    var workDayList = exports.workingDays(element.country, from, period)
    // By default start increment leave day at 0
    var leaveDays = 0
    // define the daysOff events
    var leaveEvents = []
    if (element.leave.daysOff.length !== 0) {
      var getWorkDays = workDayList
      element.leave.daysOff.forEach(function (dayOff) {
        var check = moment(dayOff, 'DD-MM-YYYY')
        var periodDayOff = check.format('M')
        // Check if the day off is in the period
        if (period === periodDayOff) {
          // Add the days Off to the daysOff events
          leaveEvents.push(moment(dayOff, 'DD-MM-YYYY').format('DD-MM-YYYY'))
          var removeDay = moment(dayOff, 'DD-MM-YYYY').format('DD-MM-YYYY')
          // Count the days Off
          leaveDays = leaveDays + 1
          var i = getWorkDays.indexOf(removeDay)
          if (i !== -1) {
          // Remove the days Off from the work events reference
            getWorkDays.splice(i, 1)
          }
        }
      })
      return {
        'leave': leaveDays,
        'total': getWorkDays.length,
        'workEvents': getWorkDays,
        'leaveEvents': leaveEvents
      }
    } else {
      return {
        'leave': leaveDays,
        'total': workDayList.length,
        'workEvents': workDayList,
        'leaveEvents': []
      }
    }
  } catch (e) {
    throw e
  }
}

exports.formatEmployees = function (params) {
  try {
    // get employees list for the companie
    var employees = require('../../data/employees/' + params.company + '.json')
    var employeesList = employees.filter(function (x) {
    // filter the employees list
      return x.country === params.country && x.company.id === params.company
    })
    var listing = {
      'created': new Date(Date.now()).toLocaleString(),
      'companyID': params.company,
      'country': params.country,
      'count': employeesList.length,
      'employees': []
    }
    // loop on the employee list to creat the personnal work events for each employee
    employeesList.forEach(function (element) {
      var employeeDetail = {
        'id': element.id,
        'name': {
          'firstname': element.name.firstname,
          'lastname': element.name.lastname
        },
        'payroll': exports.workingDaysPerEmployees(element, params.from, params.period)
      }
      // add the employee to the output
      listing.employees.push(employeeDetail)
    })
    return listing
  } catch (e) {
    throw e
  }
}

exports.generationFromConfig = function (req, res, next) {
  try {
    // get the config file
    var configFile = require('../../config/' + req.params.configName + '.json')
    var check = moment(configFile.from, 'DD-MM-YYYY')
    var periodMonth = check.format('M')
    var getEmployees = exports.formatEmployees({country: configFile.country, company: configFile.company, from: configFile.from, period: periodMonth})
    // create the output file
    fs.writeFile('upload/' + configFile.country + '_' + configFile.company + '.json', JSON.stringify(getEmployees), function (err) {
      if (err) {
        throw err
      } else {
        res.json({version: '1.0', file: 'ok', msg: 'The work events have been generated, the file is available : upload/' + configFile.country + '_' + configFile.company + '.json'})
      }
    })
  } catch (e) {
    throw e
  }
}

exports.generationFromUrl = function (req, res, next) {
  try {
    var check = moment(req.params.from, 'DD-MM-YYYY')
    var periodMonth = check.format('M')
    var getEmployees = exports.formatEmployees({country: req.params.country, company: req.params.company, from: req.params.from, period: periodMonth})
    // create the output file
    fs.writeFile('upload/' + req.params.country + '_' + req.params.company + '.json', JSON.stringify(getEmployees), function (err) {
      if (err) {
        throw err
      } else {
        res.json({version: '1.0', file: 'ok', msg: 'The work events have been generated, the file is available : upload/' + req.params.country + '_' + req.params.company + '.json'})
      }
    })
  } catch (e) {
    throw e
  }
}
