'use strict'

// Constants
const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const app = express()
const environment = process.env.NODE_ENV

app.use(cors())

// use morgan to log requests to the console
if (environment === 'test') {
  app.use(morgan('combined'))
}
var routes = require('./routes') // importing routes
routes(app) // register routes

module.exports = app
