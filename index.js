'use strict'

// Constants
const server = require('./api')
const port = process.env.PORT || 4000

server.use(function (req, res, next) {
  res.status(404).send({url: req.originalUrl + ' Not Found'})
  res.status(500).send({url: req.originalUrl + ' Not Found'})
})

server.listen(port, function () {
  console.log('Cloudator coding task: Employee works event Service running on port %d', port)
})
